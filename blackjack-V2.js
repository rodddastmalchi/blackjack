// DEBUG //
// uncomment line below to enable console logging
//console.log = function () {}

document.getElementById('run1').addEventListener('click', playRound, loadScore);

function reload() {
    location.reload();
}

window.onload = function () {
    loadScore();
    playRound();
}

var run1000 = document.getElementById("run1000");
run1000.onclick = function () {
    for (var i = 0; i < 1000; ++i) {
        playRound(i)
    }
}

if (Polyscript === undefined) {
    var Polyscript = require('./PolyScript.js')
};

function countDeck(deck) {
    return deck.reduce(function (a, b) {
        return a + b;
    });
}


function hotOrCold(deck) {
    var lowCards = countDeck(deck.slice(1, 6));
    //console.log('low cards: ' + deck.slice(1, 6));
    var midCards = countDeck(deck.slice(6, 10));
    //console.log('high cards: ' + deck.slice(6, 10));
    var highCards = deck[0] + deck[9];

    return lowCards - highCards;
}

var cardNameMap = ['A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K'];
var cardValueMap = [11, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10];
var playerDeck = [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4];
var dealerDeck = [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4];

function shufflePlayerDeck() {
    playerDeck = [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4];
}

function shuffleDealerDeck() {
    dealerDeck = [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4];
}

var playerHand = [];
var dealerHand = [];

var card = function (cardIndex) {
    this.name = cardNameMap[cardIndex];
    this.value = cardValueMap[cardIndex];
}

var cardDisplay = document.getElementById('cards');
var cardDisplayHouse = document.getElementById('cards2');

function hit(hand, deck) {
    var cardPicked = Math.floor(Math.random() * countDeck(deck));

    var cardIndex = 0;
    for (var i = 0; i < deck.length; i++) {
        cardPicked -= deck[i];

        if (cardPicked < 0) {
            cardIndex = i;
            deck[i]--;
            break;
        }
    }

    hand.push(new card(cardIndex));
}

function handValue(hand) {
    var aces = 0;
    var value = 0;
    for (var i = 0; i < hand.length; i++) {
        if (hand[i].value == 11) {
            aces++;
        }
        value += hand[i].value;
    }

    while (value > 21 && aces > 0) {
        value -= 10;
        aces--;
    }

    return value;
}

function playerDecidesToHit(hand, deck) {
    var handData = new Polyscript.StackBlock();
    handData._stack.name = "hand";
    hand.forEach(function (card) {
        var cardData = new Polyscript.StackBlock();
        cardData._stack.name = card.name;
        cardData._stack.list.push(new Polyscript.NumberBlock(card.value));
        handData._stack.list.push(cardData);

    });

    var deckData = new Polyscript.StackBlock();
    deckData._stack.name = "cards in deck";
    deck.forEach(function (cardCount) {
        deckData._stack.list.push(new Polyscript.NumberBlock(cardCount));
    });

    //console.log(handData.toStringVerbose());
    //console.log(deckData.toStringVerbose());
    /*
    var lowCards = countDeck(deck.slice(1, 6));
    var midCards = countDeck(deck.slice(6, 9));
    var highCards = deck[0] + deck[9];

    return lowCards - highCards;
    */
    var workspace = '@Poly { ->$deck ->$hand }\n\
@handValue { ->$hand $hand @isAce filter count 0 $hand @cardValue map @+ fold fixBustWithAces }\n\
@isAce { cardValue 11 = }\n\
@cardValue { 1 elem }\n\
@+ { + }\n\
@fixBustWithAces { ->$value ->$aceCount $value 21 > $aceCount 0 > & @fixBustLoop @value if }\n\
@fixBustLoop { $aceCount 1 - $value 10 - fixBustWithAces }\n\
@value { $value }\n\
@hotOrCold { ->$deck $deck 2 7 addRange $deck 10 13 addRange $deck 1 elem + - }\n\
@addRange { ->$z ->$y ->$x 0 @addElement $y $z iter }\n\
@addElement { ->$y $x $y elem + }\n\
';
    console.log(workspace);
    var runtime = Polyscript.VirtualMachine.runtime;
    runtime.reset();
    runtime.workspace = Polyscript.Workspace.deserialize(workspace)

    runtime.mainProgram.insertInProgram(runtime.workspace.getStack('hotOrCold'));
    runtime.LoadInputList([deckData]);

    console.log(Polyscript.VirtualMachine.runtime.EvaluateFully(true));

    var value = handValue(hand);
    var hotness = hotOrCold(deck);
    // console.log(hotness);

    // return value < 17;

    // if (hotness > 10)
    // {
    //   return value < 18;
    // }
    // else if (hotness > 7)
    // {
    //   return value < 17;
    // }
    // else if (hotness > 3)
    // {
    //   return value < 16;
    // }
    // else if (hotness > -6)
    // {
    //   return value < 15;
    // }
    // else if (hotness > -10)
    // {
    //   return value < 14;
    // }
    // else {
    //   return value < 13;
    // }

    if (hotness >= 11) {
        return value < 18;
    } else if (hotness >= 7) {
        return value < 17;
    } else if (hotness >= 2) {
        return value < 16;
    } else if (hotness >= -4) {
        return value < 16;
    } else if (hotness >= -9) {
        return value < 15;
    } else {
        return value < 13;
    }
    /*
    var sum_deck = 0;
    for (var i = 0; i < deck.length; i++) {
        sum_deck = sum_deck + deckWeight[deck[i]];
    }
    console.log("deckvalue" + sum_deck)
    if (sum_deck >= 2 && sum_deck < 7) {
        modNumber = 16;
    } else if (sum_deck <= -5 && sum_deck > -10) {
        modNumber = 15;
    } else if (sum_deck >= 7 && sum_deck < 11) {
        modNumber = 17;
    } else if (sum_deck <= -10) {
        modNumber = 13;
    } else if (sum_deck >= 11) {
        modNumber = 18;
    } else {
        modNumber = 16;
    }
    */
}

var hotWinRatio = {};

function loadScore() {
    console.log("track loadScore")
    var winGame = JSON.stringify(record.win);
    var loseGame = JSON.stringify(record.loss);
    var tieGame = JSON.stringify(record.tie);
    var bustGame = JSON.stringify(record.bust);
    document.getElementById("win").innerHTML = winGame;
    document.getElementById("loss").innerHTML = loseGame;
    document.getElementById("bust").innerHTML = bustGame;
    document.getElementById("tie").innerHTML = tieGame;
}

function playRound() {
    playerHand = [];
    dealerHand = [];
    //var playerHandValue = document.getElementsByClassName('card');


    //hit(dealerHand, dealerDeck); // second card is hidden; so only hit once at this point
    hit(dealerHand, playerDeck);

    hit(playerHand, playerDeck);
    hit(playerHand, playerDeck);

    // console.log('starting hand: ' + JSON.stringify(playerHand));
    while (handValue(playerHand) < 21 && playerDecidesToHit(playerHand, playerDeck)) {
        hit(playerHand, playerDeck);
        // console.log('hand: '+ JSON.stringify(playerHand));
    }
    // console.log('final hand: ' + JSON.stringify(playerHand));
    //for (var i=0; i < playerHand.length; i++) {

    //}
    cardDisplay.innerHTML = "";
    cardDisplayHouse.innerHTML = "";

    //var myCards = makeCards(i);
    //cardString = eventlist.toString().replace(/"/g, "");



    var result;
    var playerHandValue = handValue(playerHand);
    var hotness = hotOrCold(playerDeck);
    if (hotWinRatio[hotness] == undefined) {
        hotWinRatio[hotness] = {
            win: 0,
            loss: 0,
            bust: 0,
            tie: 0,
            ratio: 0
        };
    }

    if (playerHandValue > 21) {
        result = 'bust';

        hotWinRatio[hotness].bust++;
    } else {
        while (handValue(dealerHand) < 17) {
            hit(dealerHand, playerDeck);
        }

        for (var i = 0; i < playerHand.length; i++) {
            var newCardElement = document.createElement('figure');
            newCardElement.classList.add('card');
            var myCards = JSON.stringify(playerHand[i].name);
            var cardString = new String();
            cardString = myCards.toString().replace(/"/g, "");
            newCardElement.innerText = cardString;
            cardDisplay.appendChild(newCardElement);
        }


        for (var j = 0; j < dealerHand.length; j++) {
            var newCardElementHouse = document.createElement('figure');
            newCardElementHouse.classList.add('card2');
            var houseCards = JSON.stringify(dealerHand[j].name);
            var cardHouse = new String();
            cardHouse = houseCards.toString().replace(/"/g, "");
            newCardElementHouse.innerHTML = cardHouse;
            cardDisplayHouse.appendChild(newCardElementHouse);
        }

        var dealerHandValue = handValue(dealerHand);

        if (dealerHandValue > 21 || playerHandValue > dealerHandValue) {
            result = 'win';
            hotWinRatio[hotness].win++;
        } else if (playerHandValue == dealerHandValue) {
            result = 'tie';
            hotWinRatio[hotness].tie++;
        } else {
            result = 'loss';
            hotWinRatio[hotness].loss++;
        }
    }



    if (countDeck(playerDeck) < 13) {
        shufflePlayerDeck();
    }

    if (countDeck(dealerDeck) < 13) {
        shuffleDealerDeck();
    }

    hotWinRatio[hotness].ratio = hotWinRatio[hotness].win / (hotWinRatio[hotness].win + hotWinRatio[hotness].loss + hotWinRatio[hotness].bust);

    return result;
}

var record = {
    win: 0,
    loss: 0,
    bust: 0,
    tie: 0
};

for (var i = 0; i < 1; i++) {
    record[playRound()]++;
}

console.log("record" + record);
console.log(hotWinRatio);
