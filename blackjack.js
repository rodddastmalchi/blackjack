function loadScore() {
    document.getElementById("P1").innerHTML = counterPlayer;
    document.getElementById("P2").innerHTML = counterComp;
    document.getElementById("GP").innerHTML = gp;
    document.getElementById("tie").innerHTML = tie;
}

var run1000 = document.getElementById("run1000");
run1000.onclick = function () {
    for (var i = 0; i < 1000; ++i) {
        deal(i)
    }
}
window.onload = function () {
    loadScore();
}
// DEBUG //
// uncomment line below to enable console logging
//console.log = function () {}



var deckWeight = {
    '2': 1,
    '3': 1,
    '4': 1,
    '5': 1,
    '6': 1,
    '7': 0,
    '8': 0,
    '9': 0,
    '10': -1,
    'J': -1,
    'Q': -1,
    'K': -1,
    'A': -1
};



var deck2 = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];

function fillDeck() {
    deck = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']
}

function fillDeck2() {
    deck2 = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']
}
var counterPlayer = 0;
var counterComp = 0;
var tie = 0;
var gp = 0;

var deck = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];

var cardDisplay = document.getElementById('cards');

document.getElementById('run1').addEventListener('click', deal);


function reset() {
    counterPlayer = 0;
    counterComp = 0;
    tie = 0;
    gp = 0;
    loadScore()
}

function deal() {
    if (deck.length < 13) {
        fillDeck();
    }
    if (deck2.length < 13) {
        fillDeck2();
    }
    var currentCards2 = document.getElementsByClassName('card2');
    for (var i = currentCards2.length - 1; i > -1; i--) {
        card2Display.removeChild(currentCards2[i]);
    }

    var currentCards = document.getElementsByClassName('card');
    for (var i = currentCards.length - 1; i > -1; i--) {
        cardDisplay.removeChild(currentCards[i]);
    }
    addCard2();
    var totalValue2 = checkGameStatusComp()
    addCard();
    var totalValue = checkGameStatus()

    console.log("playertotal" + totalValue)
    console.log("comptotal" + totalValue2)
    console.log("deck" + deck)
    checkScore(totalValue, totalValue2)


}

function total() {
    var currentCards = document.getElementsByClassName('card');
    var totalValue = 0;
    console.log("lengthP" + currentCards.length)
    console.log("total beforeP" + totalValue)
    for (var i = 0; i < currentCards.length; i++) {
        totalValue = totalValue + getCardValue(currentCards[i]);
    }
    return totalValue;
}

function addCard() {
    var randomNumber = Math.floor(Math.random() * deck.length);
    var newCard = deck.splice(randomNumber, 1);
    var newCardElement = document.createElement('figure');
    newCardElement.classList.add('card');
    newCardElement.innerText = newCard;
    cardDisplay.appendChild(newCardElement);
}

function deckTotal() {
    var deckLength = deck.length;
    var sum = 0;
    var deckVal;
    for (var i = 0; i < deckLength; i++) {
        if (deck[i] === 'A') {
            deckVal = 1;
        } else if (deck[i] === 'J' || deck[i] === 'Q' || deck[i] === 'K') {
            deckVal = 10;
        } else {
            deckVal = Number(deck[i])
        }
        sum = sum + deckVal;
    }
    return sum;
    console.log("deckVal" + sum)
}

function checkGameStatus() {
    var countType = 2;
    var done = 0;
    var sum = deckTotal();
    var deckL = deck.length;
    average = sum / deckL;
    console.log("average" + average);

    if (countType == 0) {
        //find for average
        if (average > 7.5) {
            modNumber = 16;
        } else if (average < 5.5) {
            modNumber = 18;
        } else {
            modNumber = 17;
        }


    } else if (countType == 1) {
        //search for specific cards
        if (deck.includes('Q', 'J', '10' || 'K', 'Q', '10' || 'K', 'Q', 'J') && deck.length < 25) {
            console.log("facecards")
            modNumber = 14;
            done = 0;
        } else if (deck.includes('Q', 'J', '10' || 'K', 'Q', '10' || 'K', 'Q', 'J' || 'Q', 'J' || 'Q', 'K' || 'Q', '10' || 'K', '10' || 'K', 'J' || 'J', '10') === false) {
            console.log("no face cards")
            modNumber = 17;
            done = 0;
        } else if (deck.includes('2', '3', '4', '5' || 'A', '2', '3', '4' || '2', '3', '5' || 'A', '3', '4', '5' || '2', '3', '4', '5') && deck.length < 25) {
            console.log("low cards")
            modNumber = 17;
            done = 0;
        } else {
            modNumber = 16;
            done = 0;
        }
    } else if (countType == 2) {
        //search for most frequent card
        var freq = 1;
        var m = 0;
        var item;
        for (var i = 0; i < deck.length; i++) {
            for (var j = i; j < deck.length; j++) {
                if (deck[i] == deck[j]) {
                    m++;
                }
                if (freq < m) {
                    freq = m;
                    item = deck[i];
                }
            }
            m = 0;
        }
        console.log("most freq = " + item)
        if ((item == '2' || item == '3' || item == '4' || item == '5') && deck.length < 25) {
            modNumber = 17;
        } else if ((item == '10' || item == 'J' || item == 'Q' || item == 'K' || item == 'A') && deck.length < 25) {
            modNumber = 13;
        } else {
            modNumber = 16;
        }

    } else if (countType == 3) {
        //deck is hot or cold
        var sum_deck = 0;
        for (var i = 0; i < deck.length; i++) {
            sum_deck = sum_deck + deckWeight[deck[i]];
        }
        console.log("deckvalue" + sum_deck)
        if (sum_deck >= 2 && sum_deck < 7) {
            modNumber = 16;
        } else if (sum_deck <= -5 && sum_deck > -10) {
            modNumber = 15;
        } else if (sum_deck >= 7 && sum_deck < 11) {
            modNumber = 17;
        } else if (sum_deck <= -10) {
            modNumber = 13;
        } else if (sum_deck >= 11) {
            modNumber = 18;
        } else {
            modNumber = 16;
        }
    }

    while (done == 0) {
        addCard()
        var totalValue = total()
        console.log("modNum" + modNumber)
        if (totalValue == 21) {
            done = 1;
        } else if (totalValue > 21) {
            done = 1;

        } else if (totalValue >= modNumber && totalValue < 21) {
            done = 1;
        } else {
            done = 0;
        }

    }
    console.log("---totalvalue" + totalValue)
    return totalValue;
}

function getCardValue(cardElement) {
    var cardValue = 0;
    if (cardElement.innerText === 'J' || cardElement.innerText === 'Q' || cardElement.innerText === 'K') {
        cardValue = 10;

    } else if (cardElement.innerText === 'A') {
        cardValue = 11;


    } else {
        cardValue = Number(cardElement.innerText);

    }
    console.log("cardP" + cardValue);
    return cardValue;
}



///////////////////////////////////////HOUSE/////////////////////////////////////////


var result2Element = document.getElementById('result2');
var card2Display = document.getElementById('cards2');

function addCard2() {
    var randomNumber = Math.floor(Math.random() * deck2.length);
    var newCard2 = deck2.splice(randomNumber, 1);
    var newCard2Element = document.createElement('figure');
    newCard2Element.classList.add('card2');
    newCard2Element.innerText = newCard2;
    card2Display.appendChild(newCard2Element);
    return newCard2;
}

function totalHouse() {
    var currentCards2 = document.getElementsByClassName('card2');
    var totalValue2 = 0;
    for (var i = 0; i < currentCards2.length; i++) {
        totalValue2 = totalValue2 + getCard2Value(currentCards2[i]);
    }
    return totalValue2;
}

function checkGameStatusComp() {
    var doneHouse = 0;
    while (doneHouse == 0) {
        addCard2()
        var totalValue2 = totalHouse()
        if (totalValue2 === 21) {
            doneHouse = 1;

        } else if (totalValue2 >= 16 && totalValue2 < 21) {
            doneHouse = 1;

        } else if (totalValue2 > 21) {
            doneHouse = 1;

        } else if (totalValue2 < 16) {
            doneHouse = 0;
        }
    }
    console.log("----totalvalue2" + totalValue2)
    return totalValue2;

}

function getCard2Value(card2Element) {
    var cardValue2 = 0;

    if (card2Element.innerText === 'J' || card2Element.innerText === 'Q' || card2Element.innerText === 'K') {
        cardValue2 = 10;

    } else if (card2Element.innerText === 'A') {
        cardValue2 = 11;

    } else {
        cardValue2 = Number(card2Element.innerText);

    }
    console.log("card" + cardValue2);
    return cardValue2;
}

function checkScore(val1, val2) {
    document.getElementById("P1").innerHTML = counterPlayer;
    document.getElementById("P2").innerHTML = counterComp;
    document.getElementById("GP").innerHTML = gp;
    document.getElementById("tie").innerHTML = tie;
    var totalValue = val1;
    var totalValue2 = val2;

    if (totalValue > 21 && totalValue2 == 21) {
        console.log("totalValue > 21 && totalValue2 == 21")
        result2.innerText = 'HOUSE - BLACKJACK, HOUSE WINS';
        counterComp += 1;
        gp += 1;
        console.log("print gp" + gp)
        loadScore()
    } else if (totalValue < 21 && totalValue2 == 21) {
        console.log("totalValue > 21 && totalValue2 == 21")
        result2.innerText = 'HOUSE - BLACKJACK, HOUSE WINS';
        counterComp += 1;
        gp += 1;
        console.log("print gp" + gp)
        loadScore()
    } else if (totalValue == 21 && totalValue2 > 21) {
        console.log("totalValue ==21 && totalValue2 <21")
        result2.innerText = 'PLAYER - BLACKJACK,  YOU WIN';
        counterPlayer += 1;
        gp += 1;
        console.log("print gp" + gp)
        loadScore()
    } else if (totalValue == 21 && totalValue2 == 21) {
        console.log("totalValue ==21 && totalValue2 <21")
        result2.innerText = 'PUSH';
        tie += 1;
        gp += 1;
        console.log("print gp" + gp)
        loadScore()
    } else if (totalValue > 21 && totalValue2 > 21) {
        result2.innerText = 'PLAYER BUST, HOUSE WINS';
        counterComp += 1;
        gp += 1;
        console.log("print gp" + gp)
        loadScore()
    } else if (totalValue > 21 && totalValue2 < 21) {
        console.log("totalValue ==21 && totalValue2 <21")
        result2.innerText = 'PLAYER - BUST,  HOUSE WIN';
        counterComp += 1;
        gp += 1;
        console.log("print gp" + gp)
        loadScore()
    } else if (totalValue == 21 && totalValue2 < 21) {
        console.log("totalValue ==21 && totalValue2 <21")
        result2.innerText = 'PLAYER - BLACKJACK,  YOU WIN';
        counterPlayer += 1;
        gp += 1;
        console.log("print gp" + gp)
        loadScore()
    } else if (totalValue < 21 && totalValue2 < 21 && totalValue > totalValue2) {
        console.log("totalValue <21 && totalValue2 <21")
        result2.innerText = 'YOU HAVE HIGHER CARD VALUE - YOU WIN';
        counterPlayer += 1;
        gp += 1;
        console.log("print gp" + gp)
        loadScore()
    } else if (totalValue < 21 && totalValue2 > 21) {
        console.log("totalValue <21 && totalValue2 >21")
        result2.innerText = 'HOUSE BUST, YOU WIN'
        counterPlayer += 1;
        gp += 1;
        console.log("print gp" + gp)
        loadScore()
    } else if (totalValue < 21 && totalValue2 < 21 && totalValue == totalValue2) {
        result2.innerText = 'PUSH';
        tie += 1;
        gp += 1;
        console.log("print gp" + gp)
        loadScore()
    } else if (totalValue < 21 && totalValue2 < 21 && totalValue2 > totalValue) {
        console.log("totalValue < totalValue2")
        result2.innerText = 'HOUSE HAS HIGHER CARD VALUE - HOUSE WINS';
        counterComp += 1;
        gp += 1;
        console.log("print gp" + gp)
        loadScore()
    } else {
        console.log("error")
        result2.innerText = 'ERROR'
        gp += 1;
    }
}
